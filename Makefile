test: sharedlib.so sharedlib_loader.bin

sharedlib.so: sharedlib.o
	ld -dll sharedlib.o -o sharedlib.so

sharedlib_loader.bin: sharedlib_loader.o
	ld sharedlib_loader.o -o sharedlib_loader.bin

sharedlib.o: sharedlib.asm
	nasm -f elf -o sharedlib.o sharedlib.asm

sharedlib_loader.o: sharedlib_loader.asm
	nasm -f elf -o sharedlib_loader.o sharedlib_loader.asm

clean:
	rm -f sharedlib.o sharedlib_loader.o 