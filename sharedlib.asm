bits 32

%define SYS_WRITE 4
%define SYS_EXIT 1

section .text

global _start

_start:        
        
global _test:function
_test:
        mov ebx, 1
        mov ecx, HelloString
        mov edx, lHelloString
        mov eax, SYS_WRITE
        int 0x80

        mov eax, SYS_EXIT
        int 0x80

        
section .data
        HelloString: db "Hello from Shared Lib!", 0xA
        lHelloString: equ $-HelloString

        