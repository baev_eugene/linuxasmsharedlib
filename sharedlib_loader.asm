bits 32

%define SYS_WRITE 4
%define SYS_EXIT 1
%define SYS_USELIB 86

section .text

global _start

_start:
        mov ebx, 1
        mov ecx, HelloString
        mov edx, lHelloString
        mov eax, SYS_WRITE
        int 0x80

        mov ebx, libPath
        mov eax, SYS_USELIB
        int 0x80
        
        mov eax, SYS_EXIT
        int 0x80


section .data
        libPath:      db "./sharedlib.so"
        HelloString: db 0xA, "Shared Lib test loader", 0xA
        lHelloString: equ $-HelloString
